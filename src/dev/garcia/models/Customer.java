package dev.garcia.models;

import java.util.ArrayList;

public class Customer extends User {
	
	ArrayList<bankAccount> currAccounts = new ArrayList<bankAccount>();
	
	public Customer(String username, String password) {
		super(username, password);
	}
	
	public Customer(String username, String password, ArrayList<bankAccount> currAccounts) {
		super(username, password);
		this.currAccounts = currAccounts;
	}
	
	
	public void setBalance(int accountNumber, double balanceAmount) {
		currAccounts.get(accountNumber).amount = balanceAmount;
		
	}
	
	public double getBalance(int accountNumber) {
		return currAccounts.get(accountNumber).amount;
	}
	
	public int getAccountSize() {
		return currAccounts.size();
	}
}
