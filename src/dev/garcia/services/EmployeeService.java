package dev.garcia.services;

import java.util.Scanner;

public class EmployeeService {
	static Scanner scanner = new Scanner(System.in);

	public void employeeFlow(){
		System.out.println("Welcome back employee! Would you like to (1) Approve or reject some account, (2) View a customer's bank account, or (3) View the log of all transactions or (4) End the session?");
		String selection = scanner.next();
		switch(selection) {
		case "1":
			accountChecker();
			employeeFlow();
		case "2":
			accountView();
			employeeFlow();
		case "3":
			logView();
			employeeFlow();
		case "4":
			System.out.println("Goodbye!");
			break;
		default:
			System.out.println("Invalid input. Please try again.");
			employeeFlow();
		}
	}
	
	
	public void accountChecker() {
		System.out.println("Currently listing all pending accounts. We will iterate through each one at a time and you can choose to accept, reject, or close the session.");
		
	}
	
	public void accountView() {
		System.out.println("Please type the username of the customer's accounts you wish to view.");
		String username = scanner.next();
		
	}
	
	public void logView() {
		System.out.println("Printing the log of all transactions!");
		
	
	}

}
