package dev.garcia.services;

import java.util.Scanner;

public class CustomerService {
	
	static Scanner scanner = new Scanner(System.in);
	
	
	public void existingCustomerFlow() {
		System.out.println("Welcome back! Would you like to (1) View the balance of an account, (2) Withdrawal an amount, or (3) Deposit an amount or (4) End the session?");
		String selection = scanner.next();
		switch(selection) {
		case "1":
			balanceView();
			existingCustomerFlow();
		case "2":
			withdrawal();
			existingCustomerFlow();
		case "3":
			deposit();
			existingCustomerFlow();
		case "4":
			System.out.println("Goodbye!");
			break;
		default:
			System.out.println("Invalid input. Please try again.");
			existingCustomerFlow();
		}
	}
	
	public void newCustomerFlow() {
		System.out.println("Hello! Our system says you are a new customer! Would you like to apply for a new bank account? (Y/N)");
		String selection = scanner.next();
		if(selection == "Y" || selection == "y") {
			System.out.println("You have " + currCustomer.getAccountSize() + "accounts. Which one would you like to access.");
			int accountNumber = scanner.nextInt();
			System.out.println("What is your preferred starting balance?");
			double startBalance = scanner.nextDouble();
			if(startBalance >= 0) {
				currCustomer.setBalance(startBalance);
				System.out.println("Your account has been created, but it must be accepted by the bank first before you can access it.");
				System.out.println("Please come back again once your account is active!");
			}
		}
		else
			System.out.println("You won't be added to our system! Thank you, come again!");
	}
	
	public void balanceView(){
		System.out.println("You have " + currCustomer.getAccountSize() + " accounts. Which one would you like to view.");
		int accountNumber = scanner.nextInt() - 1;
		System.out.println("Your current balance for this account is " + toString(currCustomer.getBalance(accountNumber)));
		
	}
	
	public void withdrawal(){
		System.out.println("You have " + currCustomer.getAccountSize() + " accounts. Which one would you like to access.");
		int accountNumber = scanner.nextInt() - 1;
		System.out.println("How much would you like to withdrawal?");
		double withdrawalAmount = scanner.nextDouble();
		if(validWithdrawal(currCustomer.getBalance(accountNumber), withdrawalAmount) == true) {
			currCustomer.setBalance(currCustomer.getBalance(accountNumber) - withdrawalAmount);
			System.out.println("Money has been withdrawn successfully.");
		}
		else {
			System.out.println("Invalid withdrawal! Please try again with a new withdrawal amount.");
		}
			
	}
	
	public void deposit(){
		System.out.println("You have " + currCustomer.getAccountSize() + "accounts. Which one would you like to access.");
		int accountNumber = scanner.nextInt() - 1;
		System.out.println("How much would you like to deposit?");
		double depositAmount = scanner.nextDouble();
		if(depositAmount >= 0) {
			currCustomer.setBalance(accountNumber, currCustomer.getBalance(accountNumber) + depositAmount);
			System.out.println("Money has been deposited successfully.");
		}
		else 
			System.out.println("Invalid deposit! Please try again with a new deposit amount.");
	}
	
	public boolean validWithdrawal(double balanceAmount, double withdrawalAmount) {
		if((balanceAmount - withdrawalAmount < 0) || (withdrawalAmount < 0))
			return false;
		return true;
	}
}
